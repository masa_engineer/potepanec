require 'rails_helper'

RSpec.feature "Products Show", type: :feature do
  let(:product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "User accesses show page" do
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end
end
